import yaml
import discord
from discord.ext import commands
from discord.ext.commands import CommandNotFound, MissingRequiredArgument, BadArgument
import os
import sched, time
import asyncio
config = yaml.safe_load(open('config.yml'))
bot = commands.Bot(config['prefix'],description='', self_bot=True)

@bot.event
async def on_message(message):
    if message.author.id == bot.user.id or message.author.id == config["ownerid"]:
        await bot.process_commands(message)

async def on_command_error(ctx, error):
    if isinstance(error, commands.errors.CommandNotFound):
        pass
    elif isinstance(error, commands.errors.CheckFailure):
        await ctx.send(bot.bot_prefix + "Du hast keine Berechtigung diesen Befehl auszuführen.")
    elif isinstance(error, commands.errors.MissingRequiredArgument):
        formatter = commands.formatter.HelpFormatter()
        help = await formatter.format_help_for(ctx, ctx.command)
        await ctx.send(bot.bot_prefix + "Dir fehlen benötigte Argumente.\n" + help[0])
    elif isinstance(error, commands.errors.BadArgument):
        await ctx.send(bot.bot_prefix + "Ein Argument ist nicht gültig.")
    else:
        if _silent:
            await ctx.send(bot.bot_prefix + "Ein Fehler ist beim `{}` Befehl aufgetreten.".format(ctx.command.name))
        else:
            await ctx.send(bot.bot_prefix + "Ein Fehler ist während dem `{}` Befehl aufgetreten, überprüfe bitte die Konsole.".format(ctx.command.name))
            print("Ignoring exception in command {}".format(ctx.command.name))
            trace = traceback.format_exception(type(error), error, error.__traceback__)
            print("".join(trace))

def stop():
    task.cancel()

loop = asyncio.get_event_loop()

@bot.command(pass_context=True)
async def angelnstart(ctx):
    guild = discord.utils.get(bot.guilds, id=228841231688531968)
    channel = discord.utils.get(guild.channels, name="meer")
    task = loop.create_task(farm(channel, 60))
@bot.command(pass_context=True)
async def farmingstart(ctx):
    guild = discord.utils.get(bot.guilds, id=228841231688531968)
    channel = discord.utils.get(guild.channels, name="unternehmen-" + bot.user.name.lower())
    task = loop.create_task(farm(channel, 60))
    await ctx.message.delete()

@bot.command(pass_context=True)
async def farmingstop(ctx):
    loop.call_later(1, stop)
    await ctx.message.delete()

async def farm(channel, delay):
    while True:
        async for msg in channel.history(limit=50):
            try:
                await msg.add_reaction(msg.reactions[0].emoji)
                print("reaction added")
            except:
                pass
        await asyncio.sleep(delay)

@bot.event
async def on_ready():
    print('Eingeloggt als: ' + bot.user.name)

bot.run(config['token'], bot=False)